import Foundation

protocol MemeProviding {
    associatedtype T : Memeable
    func memeProvider() -> Memes<T>
}

protocol CatMemeProviding : MemeProviding {
    associatedtype T = Cat
}

struct CatMemeProvider : CatMemeProviding {
    func memeProvider() -> Memes<Cat> {
        return Memes(memes: [.bengal])
    }
}
