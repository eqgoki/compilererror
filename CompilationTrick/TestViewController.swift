import Foundation

class TestViewController<ProviderType : MemeProviding> where ProviderType.T == Cat {
    var provider: ProviderType?
}

extension Cat : Memeable {
    func memed() {
        // rick rolled
    }
}

extension TestViewController { }

enum Cat {
    case birman
    case bengal
}

